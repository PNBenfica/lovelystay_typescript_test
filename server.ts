const pgPromise = require('pg-promise');
const R         = require('ramda');
const request   = require('request-promise');


/*
  USAGE:
    npm run test <U username>
      inserts user 'username' in the database if the user does not exist
    npm run test <L>
      lists users located in lisbon
    npm run test <S>
      stats how many user by location
*/

const args = process.argv.slice(2)
const cmdOption = args[0]



// Limit the amount of debugging of SQL expressions
const trimLogsSize : number = 200;

// Database interface
interface DBOptions
  { host      : string
  , database  : string
  , user?     : string
  , password? : string
  , port?     : number
  };

// Actual database options
const options : DBOptions = {
  user: 'lovely',
  password: 'stay',
  host: 'localhost',
  database: 'lovelystay_test',
  port: 5432
};

console.info('Connecting to the database:',
  `${options.user}@${options.host}:${options.port}/${options.database}`);

const pgpDefaultConfig = {
  promiseLib: require('bluebird'),
  // Log all querys
  query(query) {
    console.log('[SQL   ]', R.take(trimLogsSize,query.query));
  },
  // On error, please show me the SQL
  error(err, e) {
    if (e.query) {
      console.error('[SQL   ]', R.take(trimLogsSize,e.query),err);
    }
  }
};

interface GithubUsers
  { id : number
  };

const pgp = pgPromise(pgpDefaultConfig);
const db = pgp(options);




/*
  command line parser (assumes clean input)
*/
switch(cmdOption){
  case 'U':
    insertUser(args[1])
    break
    case 'L':
      listUsersByLocation('Lisbon')
      break
    case 'S':
      showLocationStats()
      break
}




function insertUser(username){
  
  db.none('CREATE TABLE IF NOT EXISTS github_users (id BIGSERIAL, login TEXT UNIQUE, name TEXT, company TEXT, location TEXT, public_repos SMALLINT)')
  .then(() => request({
    uri: 'https://api.github.com/users/' + username,
    headers: {
          'User-Agent': 'Request-Promise'
      },
    json: true
  }))
  .then((data: GithubUsers) => db.one(
    'INSERT INTO github_users (login, name, company, location, public_repos) VALUES ($[login], $[name], $[company], $[location], $[public_repos]) RETURNING id', data)
  ).then(({id}) => console.log(id))
  .then(() => process.exit(0));

}

function listUsersByLocation(location){

  db.any(
    'SELECT login FROM github_users WHERE location=$1',location
  ).then(result => console.log(result.map(user => user.login)))
  .then(() => process.exit(0));

}

function showLocationStats(){
  
  db.any(
    'SELECT location, COUNT(*) FROM github_users GROUP BY location',
  ).then(result => console.log(JSON.stringify(result)))
  .then(() => process.exit(0));

}